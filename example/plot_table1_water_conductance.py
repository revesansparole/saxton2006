"""
Water conductance
=================

Plot evolution of water conductance in the soil with increasing soil moisture
for different textures.
"""
import matplotlib.pyplot as plt
import numpy as np
from saxton2006.table1 import (bubbling_pressure, theta_1500, theta_33, theta_sat, water_conductance,
                               water_conductance_sat)

om = 2.5 / 100

data = []
for tex, clay, sand in [
    ('sand', 0.05, 0.88),
    ('silt', 0.05, 0.10),
    ('loam', 0.20, 0.40),
    ('clay', 0.50, 0.25),
]:
    th_sat = theta_sat(clay, sand, om)
    th_fc = theta_33(clay, sand, om)
    th_pwp = theta_1500(clay, sand, om)
    psi_e = bubbling_pressure(clay, sand, om)
    k_sat = water_conductance_sat(th_sat, th_fc, th_pwp)

    thetas = np.linspace(th_pwp, th_sat, 100)
    kws = [water_conductance(th, th_sat, th_fc, th_pwp, k_sat) for th in thetas]

    data.append([tex, thetas, kws])

fig, axes = plt.subplots(1, 1, figsize=(11, 6), squeeze=False)
ax = axes[0, 0]

for tex, thetas, kws in data:
    ax.plot(thetas, kws, label=tex)

ax.legend(loc='center right')
ax.set_xlabel("Soil moisture [m3.m-3]")
ax.set_ylabel("Water conductance [mm.h-1]")

fig.tight_layout()
plt.show()
